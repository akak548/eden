" Replace tab with spaces
"au BufNewFile,BufRead *.go setlocal tabstop=4
au Filetype go set tabstop=4 et sw=4

" ========== Appearances ============="
au BufEnter *.go colorscheme lucariox

" Automatically open/close Nerdtree
autocmd vimenter *.go NERDTree
let g:go_fmt_autosave = 0
let b:ale_fixers = ['gofmt', 'golint']
let g:ale_completion_enabled = 1
